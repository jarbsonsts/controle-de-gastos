<!DOCTYPE html>
<html>

<head>
    <!-- Standard Meta -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <!-- Site Properties -->
    <title>Controle de Gastos</title>

    <link rel="stylesheet" type="text/css" href="semantic/semantic.min.css">
    <script src="semantic/jquery.min.js"></script>

    <script>
        function modalAddSalario() {
            $('#add_salario')
                .modal('show')
                ;
        }
        function modalAddAcrescimo() {
            $('#add_acrescimo')
                .modal('show')
                ;
        }
        function modalGastei() {
            $('#gastei-sa')
                .modal('show')
                ;
        }
        function relatorioMensal() {
            $('#r-mensal')
                .modal('show')
                ;
        }
    </script>

</head>

<body>
    </br>
    <div class="ui text container">
        <div class="ui segment">
            <div class="ui secondary menu">
                <button class="ui button" onclick="relatorioMensal()">Relatório Mensal</button>

                <div class="right menu">
                    <button class="ui green button" onclick="modalAddSalario()">Cadastrar Salário</button>
                    <button class="ui olive button" onclick="modalAddAcrescimo()">Aumentar Salário</button>
                </div>
            </div>
        </div>
    </div>

    </br>

    <div class="ui text container">
        <!-- INFORMAÇÂO DA DATA -->
        <?php
        include_once("conexao.php");

        date_default_timezone_set('America/Sao_Paulo');
        $data = date('d/m/y');
        $vmes = date('n');
        $ano = date('Y');

        if($vmes==1){
            $mes='Janeiro';
        }else if($vmes==2){
            $mes='Fevereiro';
        }else if($vmes==3){
            $mes='Março';
        }else if($vmes==4){
            $mes='Abril';
        }else if($vmes==5){
            $mes='Maio';
        }else if($vmes==6){
            $mes='Junho';
        }else if($vmes==7){
            $mes='Julho';
        }else if($vmes==8){
            $mes='Agosto';
        }else if($vmes==9){
            $mes='Setembro';
        }else if($vmes==10){
            $mes='Outubro';
        }else if($vmes==11){
            $mes='Novembro';
        }else if($vmes==12){
            $mes='Dezembro';
        };

        $consulta_data = $link->query("SELECT salario, acrescimo, saldo FROM mes WHERE ano =".$ano." AND mes=".$vmes."");
        while ($linha = $consulta_data->fetch(PDO::FETCH_ASSOC)) {
            $salario=(float)$linha['salario'];
            $acrescimo=(float)$linha['acrescimo'];
            $saldo_status=(float)$linha['saldo'];
        };
        


        echo '<div class="ui segment">
            <center>
                <div class="ui horizontal list">
                    <div class="item">
                        <a class="ui large grey label">MÊS</a>
                        <a class="ui large blue label">'.$mes.'</a>
                    </div>
                    <div class="item">
                        <a class="ui large grey label">DATA</a>
                        <a class="ui large blue label">'.$data.'</a>
                    </div>
                </div>
            </center>
        </div>';
        //TABELA
        echo '<table class="ui blue table">
            <thead class="full-width">
                <tr>
                    <th>Horário</th>
                    <th>Categoria</th>
                    <th>Valor</th>
                    <th><center>Editar / Apagar</center></th>
                </tr>
            </thead>
            
            <tbody>';

            // BUSCA VALORES GASTOS DIARIAMENTE
            $data_d =date('Y-m-d');
            $consulta_gastos= $link->query("SELECT id_despesas, categoria, valor, hora FROM despesas WHERE data_d='".$data_d."'");

            $gasto_d = 0;
            while ($linha = $consulta_gastos->fetch(PDO::FETCH_ASSOC)) {
                $id=$linha['id_despesas'];
                $categoria=$linha['categoria'];
                $valor=(float)$linha['valor'];    
                $hora=$linha['hora'];
                $gasto_d = $gasto_d+$valor;

                echo'<tr>
                    <td>'.$hora.'</td>
                    <td>'.$categoria.'</td>
                    <td> R$ '.$valor.'</td>
                    <td>
                        <center>
                        <a class="ui orange button" href="?act=upd&id='.$id.'"> Editar</a>
                        <a class="ui red button" href="?act=del&id='.$id.'">Apagar</a>
                        </center>
                    </td>
                </tr>';
            };
            $saldo = $salario+$acrescimo-$gasto_d;
            $saldo_t = $salario+$acrescimo;
                   
            echo'</tbody>
            <tfoot class="full-width">
                <tr>
                    <th colspan="4">
                        <div class="ui right floated small icon red big button" onclick="modalGastei()">
                            * GASTEI *
                        </div>
                        <a class="ui big yellow image label">
                            Total gasto hoje
                            <div class="big detail">R$ '.$gasto_d.'</div>
                        </a>';
                        if($saldo<0){
                            echo '<a class="ui big red image label">
                            Saldo negativo
                            <div class="big detail">R$ '.$saldo_status.'</div>
                        </a>';
                        }else if($saldo>0){
                            echo '<a class="ui big green image label">
                            Saldo disponivel
                            <div class="big detail">R$ '.$saldo_status.'</div>
                        </a>';
                        };
                    echo'</th>
                </tr>
            </tfoot>
        </table>';

        echo'<div class="ui grid">
        <div class="eight wide column">';
        if($saldo<0){
            echo '<div class="ui negative message">
            <div class="header">
              AVISO SALDO TOTAL
            </div>
            <p>Não gaste mais, seu saldo já no negativo!</p>
            </div>';
        }else if($saldo>0){
            echo '<div class="ui positive message">
            <div class="header">
              AVISO SALDO TOTAL
            </div>
            <p>Vai sobrar este mês, parabéns!</p>
          </div>';
        };
        echo'</div>
        <div class="eight wide column">';
        $saldo_tt = ($saldo_t*10)/100;
        if($gasto_d>$saldo_tt){
            echo '<div class="ui negative message">
            <div class="header">
              AVISO SALDO DIÁRIO
            </div>
            <p>Você gastou mais de 10% do seu salario!</p>
            </div>';
        }else if($gasto_d<$saldo_tt){
            echo '<div class="ui positive message">
            <div class="header">
                AVISO SALDO DIÁRIO
            </div>
            <p>Você está indo bem, limite não excedido!</p>
          </div>';
        };
        
        echo'</div>
        </div>';

        // Bloco if utilizado pela etapa Delete
        if (isset($_REQUEST["act"]) && $_REQUEST["act"] == "del" && $id != "") {
            $stmt = $link->prepare("DELETE FROM despesas WHERE id_despesas = ?");
            $stmt->bindParam(1, $id, PDO::PARAM_INT);

            $consulta = $link->query("SELECT valor  FROM despesas WHERE id_despesas =".$id."");
            while ($linha = $consulta->fetch(PDO::FETCH_ASSOC)) {
            $valor_apagado=$linha['valor'];
            };
            $consulta = $link->query("SELECT saldo  FROM mes WHERE ano =".$ano." AND mes=".$vmes."");
            while ($linha = $consulta->fetch(PDO::FETCH_ASSOC)) {
                $saldo_mes=$linha['saldo'];
            };

            if ($stmt->execute()) {
                // Cosulta tabela
                $upsaldo = $saldo_mes+$valor_apagado;
                $comando = $link->prepare("UPDATE mes SET saldo = :valor WHERE ano = :ano AND mes = :mes");
                $comando->bindParam(':valor', $upsaldo);
                $comando->bindParam(':ano', $ano);
                $comando->bindParam(':mes', $vmes);
                $comando->execute();

                echo "<p>Registo foi excluído com êxito</p>";
                $id = null;
                echo "<meta HTTP-EQUIV='refresh' CONTENT='0;URL=index.php'>";
            } else {
                echo "<p>Erro: Não foi possível executar a declaração sql</p>";
            };
        };
        ?>
        </br>
    </div>


    <div class="ui small test modal" id="add_salario">
        <div class="header">
        <center>
            ADICIONAR SALÁRIO MENSAL
            </center>
        </div>
        <div class="content">
            <form action="cad_salario_mes.php" class="ui form">
                <div class="field">
                    <label>Mês</label>
                    <select name="chmes" class="ui search dropdown">
                        <option value="1">Janeiro</option>
                        <option value="2">Fevereiro</option>
                        <option value="3">Março</option>
                        <option value="4">Abril</option>
                        <option value="5">Maio</option>
                        <option value="6">Junho</option>
                        <option value="7">Julho</option>
                        <option value="8">Agosto</option>
                        <option value="9">Setembro</option>
                        <option value="10">Outubro</option>
                        <option value="11">Novembro</option>
                        <option value="12">Dezembro</option>
                    </select>
                </div>
                <label>Ano</label>
                <select name="ano" class="ui search dropdown">
                    <option value="2018">2018</option>
                    <option value="2019">2019</option>
                    <option value="2020">2020</option>
                </select>
                <div class="field">
                    <label>Valor</label>
                    <input name="valor" placeholder="1200.59" type="text">
                </div>
                <div class="actions">
                    <div class="ui negative button">
                        Cancelar
                    </div>
                    <button class="ui green button" type="submit">SALVAR</button>
                </div>
            </form>
        </div>
    </div>

    <div class="ui small test modal" id="add_acrescimo">
    <div class="header">
    <center>
            ADICIONAR ACRESCIMO NO SALÁRIO MENSAL
            </center>
        </div>
    <center>
        </br>
        <div style="max-width:70%" class="ui orange message">
            <div class="header">
              AVISO!
            </div>
            <p>Depois de inserido o valor não poderá ser editado ou removido!</p>
            </div>
        <div class="content">
            <form action="up_salario_mes.php" class="ui form">
                <div class="field">

                    <label></label>
                    <input style="max-width:50%" name="valor" placeholder="300.50" type="text">
                  
                </div>
                <div class="actions">
                    <div class="ui negative button">
                        Cancelar
                    </div>
                    <button class="ui green button" type="submit">SALVAR</button>
                </div>
            </form>
        </div>
        </br>
        </center>
    </div>

    <div class="ui small test modal" id="gastei-sa">
        <div class="header">
        <center>
            ADICIONAR GASTO DIARIO
            </center>
        </div>
        <div class="content">
            <form action="cad_gasto_diario.php" class="ui form">
                <div class="field">
                    <label>Categoria</label>
                    <select name="categoria" class="ui search dropdown">
                        <option value="Transporte">Transporte</option>
                        <option value="Alimentação">Alimentação</option>
                        <option value="Lazer">Lazer</option>
                        <option value="Saúde">Saude</option>
                        <option value="Roupas">Roupas</option>
                        <option value="Mercado/Feira">Mercado/Feira</option>
                        <option value="Contas/Boletos">Contas/Boletos</option>
                        <option value="Impostos">Impostos</option>
                        <option value="Escola">Escola</option>
                        <option value="Outros">Outos</option>
                    </select>
                </div>
                <div class="field">
                    <label>Valor</label>
                    <input name="valor" placeholder="5.59" type="text">
                </div>
                <div class="actions">
                    <div class="ui negative button">
                        Cancelar
                    </div>
                    <button class="ui green button" type="submit">SALVAR</button>
                </div>
            </form>
        </div>
    </div>

    <div class="ui small test modal" id="r-mensal">
        <div class="header">
            RELATÓRIO MENSAL
        </div>
        <div class="content">
            <form action="relatorio_mensal.php" class="ui form">
                <div class="field">
                    <label>Mês</label>
                    <select name="rmes" class="ui search dropdown">
                        <option value="1">Janeiro</option>
                        <option value="2">Fevereiro</option>
                        <option value="3">Março</option>
                        <option value="4">Abril</option>
                        <option value="5">Maio</option>
                        <option value="6">Junho</option>
                        <option value="7">Julho</option>
                        <option value="8">Agosto</option>
                        <option value="9">Setembro</option>
                        <option value="10">Outubro</option>
                        <option value="11">Novembro</option>
                        <option value="12">Dezembro</option>
                    </select>
                    <label>Ano</label>
                    <select name="rano" class="ui search dropdown">
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>

                    </select>
                </div>
                <div class="actions">
                    <div class="ui negative button">
                        Cancelar
                    </div>
                    <button class="ui green button" type="submit">MOSTRAR</button>
                </div>
            </form>
        </div>
    </div>

    <script src="semantic/semantic.min.js"></script>

</body>

</html>