<?php
include_once("conexao.php");

$chmes = (int)$_GET["chmes"];
$ano = (int)$_GET["ano"]; 
$valor = (float)$_GET["valor"];
$cont=0;

$consulta = $link->query("SELECT mes, ano FROM mes");

while ($linha = $consulta->fetch(PDO::FETCH_ASSOC)) {
    if($linha['mes']==$chmes && $linha['ano']==$ano){
        $cont++;
        echo "<br /><br />O salário já foi cadastrado!<br />";
        echo "Mês: {$linha['mes']} - Ano: {$linha['ano']}<br />";
        echo "<br /><a href='http://localhost/controle'><h3>Voltar</h3></a>";
    };
};

if($cont==0){
    $comando = $link->prepare("INSERT INTO mes (mes, ano, salario, saldo) VALUES(:mes, :ano, :salario, :saldo)");
    $comando->bindParam(':mes',$chmes);
    $comando->bindParam(':ano',$ano);
    $comando->bindParam(':salario',$valor);
    $comando->bindParam(':saldo',$valor);
    $comando->execute();
    echo "<br />Salvo com sucesso!<br />";
    echo "<a href='http://localhost/controle'><h3>Voltar</h3></a>";
    echo "Ou espere 3 segundos!<br />";
    echo "<meta HTTP-EQUIV='refresh' CONTENT='3;URL=index.php'>";
};
?>


