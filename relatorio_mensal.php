<!DOCTYPE html>
<html>

<head>
    <!-- Standard Meta -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <!-- Site Properties -->
    <title>Controle de Gastos</title>

    <link rel="stylesheet" type="text/css" href="semantic/semantic.min.css">
    <script src="semantic/jquery.min.js"></script>

    <script>
        function modalAddSalario() {
            $('#add_salario')
                .modal('show')
                ;
        }
        function modalAddAcrescimo() {
            $('#add_acrescimo')
                .modal('show')
                ;
        }
        function modalGastei() {
            $('#gastei-sa')
                .modal('show')
                ;
        }
    </script>

</head>

<body>
    </br>
    <div class="ui text container">
        <div class="ui segment">
            <div class="ui secondary menu">
                <div class="right menu">
                    <button class="ui black button" onclick="location.href='index.php'">INICIO</button>
                </div>
            </div>
        </div>
    </div>

    </br>
    
    <div class="ui text container">
        <!-- INFORMAÇÂO DA DATA -->
        <?php
        include_once("conexao.php");

        date_default_timezone_set('America/Sao_Paulo');
        
        $vmes = $_GET["rmes"];
        $vano = $_GET["rano"];

        if($vmes==1){
            $rmes='Janeiro';
        }else if($vmes==2){
            $rmes='Fevereiro';
        }else if($vmes==3){
            $rmes='Março';
        }else if($vmes==4){
            $rmes='Abril';
        }else if($vmes==5){
            $rmes='Maio';
        }else if($vmes==6){
            $rmes='Junho';
        }else if($vmes==7){
            $rmes='Julho';
        }else if($vmes==8){
            $rmes='Agosto';
        }else if($vmes==9){
            $rmes='Setembro';
        }else if($vmes==10){
            $rmes='Outubro';
        }else if($vmes==11){
            $rmes='Novembro';
        }else if($vmes==12){
            $rmes='Dezembro';
        };

        echo '<div class="ui segment">
            <center>
                <div class="ui horizontal list">
                    <div class="item">
                        <a class="ui large grey label">MÊS</a>
                        <a class="ui large blue label">'.$rmes.'</a>
                    </div>
                    <div class="item">
                        <a class="ui large grey label">ANO</a>
                        <a class="ui large blue label">'.$vano.'</a>
                    </div>
                </div>
            </center>
        </div>';
        
        
            
        
        echo '<table class="ui blue table">
            <thead class="full-width">
                <tr>
                    <th>Horário</th>
                    <th>Categoria</th>
                    <th>Valor</th>
                </tr>
            </thead>
            
            <tbody>';
            // OBTENDO ID DO MES PARA FAZER RELATORIO MENSAL
            $consulta = $link->query("SELECT id_mes  FROM mes WHERE ano =".$vano." AND mes=".$vmes."");
            while ($linha = $consulta->fetch(PDO::FETCH_ASSOC)) {
            $idmes=$linha['id_mes'];
            };

            // OBTENDO DADOS TABELA MES 
            $consulta = $link->query("SELECT salario, acrescimo, saldo FROM mes WHERE ano =".$vano." AND mes=".$vmes."");
            while ($linha = $consulta->fetch(PDO::FETCH_ASSOC)) {
            $salario=$linha['salario'];
            $acrescimo=$linha['acrescimo'];
            $saldo_status=$linha['saldo'];
            };
            $salariot = $salario+$acrescimo;
            $consulta_gastos= $link->query("SELECT categoria, valor, hora FROM despesas WHERE fk_mes=".$idmes."");

            $gasto_d = 0;
            while ($linha = $consulta_gastos->fetch(PDO::FETCH_ASSOC)) {
                $categoria=$linha['categoria'];
                $valor=(float)$linha['valor'];    
                $hora=$linha['hora'];
                $gasto_d = $gasto_d+$valor;

                echo'<tr>
                    <td>'.$hora.'</td>
                    <td>'.$categoria.'</td>
                    <td> R$ '.$valor.'</td>
                </tr>';
            };

            $saldo_mes = $salariot-$gasto_d;
            echo'</tbody>
            <tfoot class="full-width">
                <tr>
                    <th colspan="4">
                        <a class="ui big yellow image label">
                            Total gasto
                            <div class="big detail">R$ '.$gasto_d.'</div>
                        </a>';
                        if($gasto_d>$salariot){
                            echo '<a class="ui big red image label">
                            Status
                            <div class="big detail">DEVENDO</div>
                        </a>';
                        }else if($gasto_d<$salariot){
                            echo '<a class="ui big green image label">
                            Status
                            <div class="big detail">NÃO GASTOU TUDO</div>
                        </a>';
                        };
                        if($saldo_status<0){
                            echo '<a class="ui big red image label">
                            Saldo negativo
                            <div class="big detail">R$ '.$saldo_status.'</div>
                        </a>';
                        }else if($saldo_status>0){
                            echo '<a class="ui big green image label">
                            Saldo disponivel
                            <div class="big detail">R$ '.$saldo_status.'</div>
                        </a>';
                        };
                    echo '</th>
                </tr>
            </tfoot>
        </table>';
        ?>


    </div>
    <script src="semantic/semantic.min.js"></script>

</body>

</html>