<?php
include_once("conexao.php");

date_default_timezone_set('America/Sao_Paulo');
$ano = date('Y');
$vmes =date('n');
$valor = (float)$_GET["valor"];
$cont=0;

$consulta = $link->query("SELECT acrescimo, saldo FROM mes WHERE ano =".$ano." AND mes=".$vmes."");
while ($linha = $consulta->fetch(PDO::FETCH_ASSOC)) {
    $acrescimo=$linha['acrescimo'];
    $saldo=$linha['saldo'];
};
$total = $valor+$acrescimo;
$total2= $valor+$saldo;

$comando = $link->prepare("UPDATE mes SET acrescimo = :valor, saldo = :saldo WHERE ano = :ano AND mes = :mes");
$comando->bindParam(':valor', $total);
$comando->bindParam(':saldo', $total2);
$comando->bindParam(':ano', $ano);
$comando->bindParam(':mes', $vmes);
$comando->execute();

echo "<br />Salvo com sucesso!<br />";
echo "<a href='http://localhost/controle'><h3>Voltar</h3></a>";
echo "Ou espere 3 segundos!<br />";
echo "<meta HTTP-EQUIV='refresh' CONTENT='3;URL=index.php'>";


?>