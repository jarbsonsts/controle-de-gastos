drop database controle_de_gastos;
create database controle_de_gastos;
use controle_de_gastos;

CREATE TABLE `mes` (
  `id_mes` int(5) primary key auto_increment,
  `mes` int(3),
  `ano` int(4),
  `salario` float,
  `acrescimo` float,
  `saldo` float
) ENGINE=InnoDB;

CREATE TABLE `despesas` (
  `id_despesas` int(5) primary key auto_increment,
  `categoria` varchar(20),
  `valor` float DEFAULT NULL,
  `data_d` date DEFAULT NULL,
  `hora` varchar(8) DEFAULT NULL,
  `fk_mes` int,
  CONSTRAINT `fk_despesas_mes` FOREIGN KEY (`fk_mes`) REFERENCES `mes` (`id_mes`)
) ENGINE=InnoDB;

aluma 
aluma 